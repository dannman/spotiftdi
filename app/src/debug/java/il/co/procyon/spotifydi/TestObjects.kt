package il.co.procyon.spotifydi

import il.co.procyon.spotifydi.uiObjects.Album
import il.co.procyon.spotifydi.uiObjects.Artist
import il.co.procyon.spotifydi.uiObjects.Header
import il.co.procyon.spotifydi.uiObjects.Track

val testResults = listOf(
        Header().apply { header.set("Albums")},
        Album("0").apply {
            albumName.set("Abbey Road")
            artistName.set("Beatles")
            coverArtUrl.set("https://upload.wikimedia.org/wikipedia/he/thumb/4/42/Beatles_-_Abbey_Road.jpg/250px-Beatles_-_Abbey_Road.jpg")
        },
        Album("1").apply {
            albumName.set("Sgt. Peppers")
            artistName.set("Beatles")
            coverArtUrl.set("https://upload.wikimedia.org/wikipedia/en/thumb/5/50/Sgt._Pepper%27s_Lonely_Hearts_Club_Band.jpg/220px-Sgt._Pepper%27s_Lonely_Hearts_Club_Band.jpg")
        },
        Album("2").apply {
            albumName.set("Help!")
            artistName.set("Beatles")
            coverArtUrl.set("https://is5-ssl.mzstatic.com/image/thumb/Music/80/c7/58/mzi.adobupfv.tif/268x0w.jpg")
        },
        Header().apply { header.set("Artists") },
        Artist("0").apply {
            artistName.set("Beatles")
            coverArtUrl.set("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSPSaVIw9mcngBnf6d8Gvvz2MKVnoIA2KTBOeBINYcNgfBWR8-")
        },
        Header().apply { header.set("Tracks") },
        Track("0").apply {
            trackName.set("Octopuses Garden")
            albumName.set("Abbey road")
            artistName.set("Beatles")
            coverArtUrl.set("https://upload.wikimedia.org/wikipedia/he/thumb/4/42/Beatles_-_Abbey_Road.jpg/250px-Beatles_-_Abbey_Road.jpg")

        },
        Track("0").apply {
            trackName.set("Because")
            albumName.set("Abbey road")
            artistName.set("Beatles")
            coverArtUrl.set("https://upload.wikimedia.org/wikipedia/he/thumb/4/42/Beatles_-_Abbey_Road.jpg/250px-Beatles_-_Abbey_Road.jpg")

        },
        Track("0").apply {
            trackName.set("Sun king")
            albumName.set("Abbey road")
            artistName.set("Beatles")
            coverArtUrl.set("https://upload.wikimedia.org/wikipedia/he/thumb/4/42/Beatles_-_Abbey_Road.jpg/250px-Beatles_-_Abbey_Road.jpg")
        }

)