package il.co.procyon.spotifydi.construction.presentation

import android.content.Context
import dagger.Module
import dagger.Provides
import il.co.procyon.spotifydi.ui.albumActivity.AlbumViewMvc
import il.co.procyon.spotifydi.ui.artistActivity.ArtistViewMvc
import il.co.procyon.spotifydi.ui.mainActivity.MainViewMvc
import il.co.procyon.spotifydi.ui.root.ViewMvcFactory


@Module
class MvcModule(context: Context) {

    private val mvcFactory = ViewMvcFactory(context)

    @Provides
    fun provideMainViewMvc(): MainViewMvc{
        return mvcFactory.newInstance(MainViewMvc::class)
    }
    @Provides
    fun provideAlbumViewMvc(): AlbumViewMvc{
        return mvcFactory.newInstance(AlbumViewMvc::class)
    }
    @Provides
    fun provideArtistViewMvc(): ArtistViewMvc{
        return mvcFactory.newInstance(ArtistViewMvc::class)
    }

}