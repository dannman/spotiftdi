package il.co.procyon.spotifydi.uiObjects

import il.co.procyon.spotifydi.networking.responses.AlbumSchema

data class AlbumUiObject(val imageUrl: String,
                         val albumTitle: String,
                         val artist: String,
                         val releaseYear: String,
                         val tracks: List<AlbumTrack>) {

    constructor(schema: AlbumSchema) : this(
            imageUrl = schema.images[0].url,
            albumTitle = schema.name,
            artist = schema.artists.foldIndexed("Artists: ") { index, acc, artist ->
                if (index != schema.artists.size - 1) {
                    "$acc ${artist.name}, "
                } else {
                    "$acc ${artist.name}"
                }
            },
            releaseYear = schema.releaseDate,
            tracks = schema.tracks.items.map { item -> AlbumTrack(item.name, item.id) }


    )

    data class AlbumTrack(val name: String, val id: String)
}