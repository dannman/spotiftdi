package il.co.procyon.spotifydi.ui.artistActivity

import il.co.procyon.spotifydi.ui.baseViewMvc.ObservableViewMvc
import il.co.procyon.spotifydi.uiObjects.ArtistUiObject
import il.co.procyon.spotifydi.uiObjects.Listable
import il.co.procyon.spotifydi.uiObjects.Selectable

interface ArtistViewMvc: ObservableViewMvc<ArtistViewMvc.Listener> {

    fun setArtistData(artist: ArtistUiObject)

    fun setArtistAlbums(albums: List<Listable>)

    interface Listener{
        fun onAlbumClicked(album: Selectable)
    }
}