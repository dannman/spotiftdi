package il.co.procyon.spotifydi.ui.mainActivity

import android.app.ActivityOptions
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.FragmentNavigator
import il.co.procyon.spotifydi.CredentialManager
import il.co.procyon.spotifydi.networking.FetchUseCase
import il.co.procyon.spotifydi.networking.responses.SearchResponseSchema
import il.co.procyon.spotifydi.networking.responses.TokenResponseSchema
import il.co.procyon.spotifydi.produceBase64AccessKey
import il.co.procyon.spotifydi.ui.albumActivity.AlbumActivityView
import il.co.procyon.spotifydi.ui.artistActivity.ArtistActivityView
import il.co.procyon.spotifydi.ui.common.BaseActivity
import il.co.procyon.spotifydi.ui.common.BaseFragment
import il.co.procyon.spotifydi.uiObjects.Album
import il.co.procyon.spotifydi.uiObjects.Artist
import il.co.procyon.spotifydi.uiObjects.Selectable
import javax.inject.Inject

class MainFragment: BaseFragment(), MainViewMvc.Listener {

    @Inject
    lateinit var viewMvc: MainViewMvc
    @Inject
    lateinit var querySearchCase: FetchUseCase<SearchResponseSchema>
    @Inject
    lateinit var getTokenCase: FetchUseCase<TokenResponseSchema>
    @Inject
    lateinit var credentialManager: CredentialManager

    override fun onCreate(savedInstanceState: Bundle?) {
        (activity as BaseActivity).presentationComponent.inject(this@MainFragment)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return viewMvc.containerView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (credentialManager.token.isValid()) {
            viewMvc.hasTokenBeenFetched(true)
        } else {
            viewMvc.hasTokenBeenFetched(false)
            onGetTokenClick()
        }
    }

    override fun onStart() {
        super.onStart()
        viewMvc.registerListener(this)
    }

    override fun onStop() {
        super.onStop()
        viewMvc.unregisterListener(this)
        querySearchCase.abortCall()
        getTokenCase.abortCall()
    }

    override fun onGetTokenClick() {
        val produceBase64AccessKey = produceBase64AccessKey("37b1e59143cf4627a476ec9133d88b65", "e19cf7d109474f1c8509ea6914e9bd51")
        getTokenCase.apply {
            setCall(spotifyApi.getToken(accessKey = produceBase64AccessKey))
            makeCall(object : FetchUseCase.ResponseListener<TokenResponseSchema> {
                override fun onResponseSuccess(response: TokenResponseSchema) {
                    viewMvc.storeToken(response.accessToken, response.expiresIn.toLong())
                    viewMvc.hasTokenBeenFetched(true)
                    credentialManager.token = CredentialManager.Token(response.accessToken, response.expiresIn.toLong())
                }

                override fun onFail() {
                    handleApiError("Error fetching token")
                    viewMvc.hasTokenBeenFetched(false)
                }

            })
        }

    }

    override fun onSearchClick(query: String, types: String) {
        querySearchCase.apply {
            setCall(spotifyApi.search(query = query, type = types))
            makeCall(object : FetchUseCase.ResponseListener<SearchResponseSchema> {
                override fun onResponseSuccess(response: SearchResponseSchema) {
                    viewMvc.bindSearchResponses(response)
                }

                override fun onFail() {
                    handleApiError("Error searching $query")
                }

            })
        }
    }

    override fun onListItemClick(selectable: Selectable) {
        Toast.makeText(context, selectable.ids, Toast.LENGTH_SHORT).show()

        //todo - rewrite navigation

//        val options = ActivityOptions.makeSceneTransitionAnimation(this, *selectable.sharedElements)
//
//        when (selectable) {
//            is Album -> startActivity(AlbumActivityView.intentBuilder(this, selectable.id), options.toBundle())
//            is Artist -> {
//                val intent = ArtistActivityView.intentBuilder(this, selectable.id)
//                intent.putExtra("size", (selectable.sharedElements[1].first as TextView).textSize)
//                startActivity(intent, options.toBundle())
//            }
//        }

        val extras = FragmentNavigator.Extras.Builder().apply {
            selectable.sharedElements.forEach {
                this.addSharedElement(it.first, it.second)
                Log.d("MainFragment", "shared elements: ${it.second}")
            }
        }.build()

        Log.d("MainFragment", "shared element num: ${extras.sharedElements}")


        val action = when(selectable){
            is Album -> MainFragmentDirections.actionMainFragmentToAlbumFragment(selectable.id)
            is Artist -> MainFragmentDirections.actionMainFragmentToArtistFragment(selectable.id)
            else -> null
        }

        action?.let {
            Navigation.findNavController(viewMvc.containerView!!).navigate(it, extras)
        }
    }
}