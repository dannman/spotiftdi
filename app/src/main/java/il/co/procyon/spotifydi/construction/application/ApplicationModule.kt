package il.co.procyon.spotifydi.construction.application

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import il.co.procyon.spotifydi.CredentialManager
import il.co.procyon.spotifydi.networking.SpotifyApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class ApplicationModule(val app: Application) {


    val context: Context
        @Provides
        get() = app.applicationContext

    @Provides
    @Singleton
    fun buildSpotifyApi(): SpotifyApi{
        val retrofit = Retrofit.Builder()
                .baseUrl(" https://api.spotify.com/v1/").addConverterFactory(GsonConverterFactory.create())
                .client(OkHttpClient().newBuilder().apply(httpClientBuilder()).build())
                .build()
        return retrofit.create(SpotifyApi::class.java)

    }

    private fun httpClientBuilder(): (OkHttpClient.Builder).() -> Unit {
        return {
            addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
        }
    }

    @Provides
    @Singleton
    fun getCredentialManager():CredentialManager{
        return CredentialManager
    }


}