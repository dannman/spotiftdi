package il.co.procyon.spotifydi.ui.albumActivity

import il.co.procyon.spotifydi.ui.baseViewMvc.ObservableViewMvc
import il.co.procyon.spotifydi.uiObjects.AlbumUiObject

interface AlbumViewMvc: ObservableViewMvc<AlbumViewMvc.Listener> {

    fun setAlbumData(data: AlbumUiObject)

    interface Listener {

    }
}