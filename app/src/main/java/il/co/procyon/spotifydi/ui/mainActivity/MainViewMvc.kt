package il.co.procyon.spotifydi.ui.mainActivity

import il.co.procyon.spotifydi.networking.responses.SearchResponseSchema
import il.co.procyon.spotifydi.ui.baseViewMvc.ObservableViewMvc
import il.co.procyon.spotifydi.uiObjects.Selectable

interface MainViewMvc: ObservableViewMvc<MainViewMvc.Listener> {

    fun bindSearchResponses(results: SearchResponseSchema)
    fun storeToken(token: String, expiresIn: Long)
    fun hasTokenBeenFetched(hasToken: Boolean)


    interface Listener{

        fun onGetTokenClick()
        fun onSearchClick(query: String, types: String)
        fun onListItemClick(selectable: Selectable )

    }
}