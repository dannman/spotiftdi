package il.co.procyon.spotifydi.common

abstract class BaseObservable<LISTENER_CLASS> ( private val listeners: MutableSet<LISTENER_CLASS> = HashSet()) {

    open fun registerListener(listener: LISTENER_CLASS){
        listeners.add(listener)
    }

    open fun unregisterListener(listener: LISTENER_CLASS){
        listeners.remove(listener)
    }

    fun getListeners():Set<LISTENER_CLASS>{
        return listeners.toSet()
    }

}