package il.co.procyon.spotifydi.construction.application

import dagger.Component
import il.co.procyon.spotifydi.construction.presentation.MvcModule
import il.co.procyon.spotifydi.construction.presentation.MvcModule_ProvideAlbumViewMvcFactory
import il.co.procyon.spotifydi.construction.presentation.PresentationComponent
import il.co.procyon.spotifydi.ui.root.ViewMvcFactory
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
public interface ApplicationComponent {

    fun newPresentationComponent(mvcModule: MvcModule): PresentationComponent
}