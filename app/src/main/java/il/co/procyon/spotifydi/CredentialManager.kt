package il.co.procyon.spotifydi

import android.util.Log

object CredentialManager {


    var token: Token = Token()


    class Token(val tokenVal: String = "", val lifespan: Long = Long.MIN_VALUE) {
        private var expirationTime = System.currentTimeMillis() + (lifespan * 1000L)
            set(value)  {
                Log.d("Token", "set expiration: now=${System.currentTimeMillis()}, expirationTime(millis)= $value")
                field = value
            }


        fun isValid(): Boolean {

            Log.d("Token", "isValid: now= ${System.currentTimeMillis()}, expirationTime(millis)= $expirationTime")
            return lifespan != Long.MIN_VALUE && expirationTime > System.currentTimeMillis()
        }
    }
}