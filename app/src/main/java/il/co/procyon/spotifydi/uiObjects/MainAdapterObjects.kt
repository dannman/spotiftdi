package il.co.procyon.spotifydi.uiObjects

import android.view.View
import androidx.databinding.ObservableField

data class Header(val header: ObservableField<String> = ObservableField("")): Listable

interface Selectable {
    val ids: String
    var transitionView: View?
    var sharedElements: Array<out android.util.Pair<View, String>>
}

interface Listable

class SelectableImpl(private val id: String) : Selectable {
    override var sharedElements: Array<out android.util.Pair<View, String>> = arrayOf()

    override var transitionView: View? = null
    override val ids: String
        get() = id
}

data class Artist(val id: String,
                  val artistName: ObservableField<String> = ObservableField(""),
                  val coverArtUrl: ObservableField<String> = ObservableField("")) : Listable, Selectable by SelectableImpl(id)

data class Album(val id: String,
                 val albumName: ObservableField<String> = ObservableField(""),
                 val artistName: ObservableField<String> = ObservableField(""),
                 val coverArtUrl: ObservableField<String> = ObservableField("")) : Listable, Selectable by SelectableImpl(id)

data class Track(val id: String,
                 val trackName: ObservableField<String> = ObservableField(""),
                 val albumName: ObservableField<String> = ObservableField(""),
                 val artistName: ObservableField<String> = ObservableField(""),
                 val coverArtUrl: ObservableField<String> = ObservableField("")) : Listable, Selectable by SelectableImpl(id)
