package il.co.procyon.spotifydi.networking.requests

import com.google.gson.annotations.SerializedName

data class TokenRequest(@SerializedName("grant_type") val grant_type: String = "client_credentials")