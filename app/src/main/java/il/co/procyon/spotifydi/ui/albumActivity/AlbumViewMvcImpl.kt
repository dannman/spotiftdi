package il.co.procyon.spotifydi.ui.albumActivity

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import il.co.procyon.spotifydi.R
import il.co.procyon.spotifydi.databinding.ActivityAlbumBinding
import il.co.procyon.spotifydi.databinding.CellTrackOnlyNameBinding
import il.co.procyon.spotifydi.ui.baseViewMvc.BaseViewMvc
import il.co.procyon.spotifydi.uiObjects.AlbumUiObject
import kotlin.properties.Delegates

class AlbumViewMvcImpl(override val containerView: View?) : BaseViewMvc<AlbumViewMvc.Listener>(), AlbumViewMvc {

    companion object {
        fun instantiateMvc(context: Context, root: ViewGroup? = null): AlbumViewMvcImpl{
            return AlbumViewMvcImpl(LayoutInflater.from(context).inflate(R.layout.activity_album, root))
        }
    }

    var binder: ActivityAlbumBinding? = null

    override fun registerListener(listener: AlbumViewMvc.Listener) {
        super.registerListener(listener)
        binder = DataBindingUtil.getBinding(containerView!!)?:ActivityAlbumBinding.bind(containerView)

    }


    override fun setAlbumData(data: AlbumUiObject) {
        binder?.album = data
    }

    class TracksAdapter : RecyclerView.Adapter<TrackViewHolder>() {

        var trackList: List<AlbumUiObject.AlbumTrack> by Delegates.observable(emptyList()) { _, _, _ ->
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackViewHolder {
            val binder = CellTrackOnlyNameBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return TrackViewHolder(binder)
        }

        override fun getItemCount(): Int {
            return trackList.size
        }

        override fun onBindViewHolder(holder: TrackViewHolder, position: Int) {
            holder.binder.apply {
                trackData = trackList[position]
                index = position
            }

        }

    }

    class TrackViewHolder(val binder: CellTrackOnlyNameBinding) : RecyclerView.ViewHolder(binder.root)
}