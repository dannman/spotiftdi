package il.co.procyon.spotifydi.ui.albumActivity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import il.co.procyon.spotifydi.networking.FetchUseCase
import il.co.procyon.spotifydi.networking.responses.AlbumSchema
import il.co.procyon.spotifydi.ui.common.BaseActivity
import il.co.procyon.spotifydi.uiObjects.AlbumUiObject
import javax.inject.Inject

class AlbumActivityView : BaseActivity(), AlbumViewMvc.Listener {

    companion object {
        private const val ID = "ID"

        fun intentBuilder(context: Context, id: String): Intent {
            return Intent(context, AlbumActivityView::class.java).apply {
                putExtra(ID, id)
            }

        }
    }

    @Inject
    lateinit var albumMvc: AlbumViewMvc

    @Inject
    lateinit var getAlbumsUseCase: FetchUseCase<AlbumSchema>

    val id: String by lazy { intent.getStringExtra(ID) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition()
        presentationComponent.inject(this)

        setContentView(albumMvc.containerView)
    }

    override fun onStart() {
        super.onStart()
        albumMvc.registerListener(this)

        getAlbumsUseCase.apply {
            setCall(spotifyApi.getAlbumInfo(albumId = id))
            makeCall(albumListener)
        }

    }

    private val albumListener = object : FetchUseCase.ResponseListener<AlbumSchema> {
        override fun onResponseSuccess(response: AlbumSchema) {
            albumMvc.setAlbumData(AlbumUiObject(response))
            startPostponedEnterTransition()
        }

        override fun onFail() {
            handleApiError("error loading album")
        }

    }

    override fun onStop() {
        super.onStop()
        albumMvc?.unregisterListener(this)
        getAlbumsUseCase.abortCall()

    }
}