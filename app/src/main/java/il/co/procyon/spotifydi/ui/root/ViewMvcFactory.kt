package il.co.procyon.spotifydi.ui.root

import android.content.Context
import dagger.Module
import il.co.procyon.spotifydi.ui.albumActivity.AlbumViewMvc
import il.co.procyon.spotifydi.ui.albumActivity.AlbumViewMvcImpl
import il.co.procyon.spotifydi.ui.artistActivity.ArtistViewMvc
import il.co.procyon.spotifydi.ui.artistActivity.ArtistViewMvcImpl
import il.co.procyon.spotifydi.ui.baseViewMvc.BaseViewMvc
import il.co.procyon.spotifydi.ui.baseViewMvc.ObservableViewMvc
import il.co.procyon.spotifydi.ui.mainActivity.MainViewMvc
import il.co.procyon.spotifydi.ui.mainActivity.MainViewMvcImpl
import javax.inject.Inject
import kotlin.reflect.KClass

class ViewMvcFactory @Inject constructor(val context: Context) {

    fun <T : ObservableViewMvc<*>> newInstance(type: KClass<T>): T {
        return when (type) {
            MainViewMvc::class -> MainViewMvcImpl.instantiateMvc(context) as T
            AlbumViewMvc::class -> AlbumViewMvcImpl.instantiateMvc(context) as T
            ArtistViewMvc::class -> ArtistViewMvcImpl.instantiateMvc(context) as T
            else -> {
                throw IllegalArgumentException("unsupported MVC view class $type")
            }
        }
    }
}