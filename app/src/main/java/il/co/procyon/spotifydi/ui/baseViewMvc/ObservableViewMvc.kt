package il.co.procyon.spotifydi.ui.baseViewMvc

import kotlinx.android.extensions.LayoutContainer

interface ObservableViewMvc<LISTENER_TYPE>:  LayoutContainer {


    fun registerListener(listener: LISTENER_TYPE)
    fun unregisterListener(listener: LISTENER_TYPE)
}