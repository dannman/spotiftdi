package il.co.procyon.spotifydi.uiObjects

import androidx.databinding.ObservableField
import il.co.procyon.spotifydi.networking.responses.ArtistSchema

data class ArtistUiObject(val imageUrl: ObservableField<String> = ObservableField(),
                          val artistName: ObservableField<String> = ObservableField(),
                          val genres: ObservableField<String> = ObservableField(),
                          val itemsList : MutableList<Listable> = mutableListOf()) {


    fun setData(schema: ArtistSchema){
        imageUrl.set(schema.images.firstOrNull()?.url ?: "null")
        artistName.set(schema.name)
        genres.set(schema.genres.foldIndexed("Genres: ") { index, acc, s ->
            if (index != schema.genres.size - 1) {
                return@foldIndexed "$s, "
            } else {
                return@foldIndexed s
            }
        })
    }

    fun appendListableItems(listable: List<Listable>){
        itemsList.addAll(listable)
    }
}