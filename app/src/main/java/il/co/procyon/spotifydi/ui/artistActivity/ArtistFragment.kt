package il.co.procyon.spotifydi.ui.artistActivity

import android.app.ActivityOptions
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.SharedElementCallback
import androidx.navigation.Navigation
import androidx.navigation.fragment.FragmentNavigator
import il.co.procyon.spotifydi.R
import il.co.procyon.spotifydi.constructAlbumSection
import il.co.procyon.spotifydi.networking.FetchUseCase
import il.co.procyon.spotifydi.networking.responses.AlbumsSearchSchema
import il.co.procyon.spotifydi.networking.responses.ArtistSchema
import il.co.procyon.spotifydi.ui.common.BaseActivity
import il.co.procyon.spotifydi.ui.common.BaseFragment
import il.co.procyon.spotifydi.uiObjects.Album
import il.co.procyon.spotifydi.uiObjects.ArtistUiObject
import il.co.procyon.spotifydi.uiObjects.Listable
import il.co.procyon.spotifydi.uiObjects.Selectable
import javax.inject.Inject

class ArtistFragment : BaseFragment(), ArtistViewMvc.Listener {

    @Inject
    lateinit var viewMvc: ArtistViewMvc

    @Inject
    lateinit var artistDetailsUseCase: FetchUseCase<ArtistSchema>
    @Inject
    lateinit var artistAlbumsUseCase: FetchUseCase<AlbumsSearchSchema>


    //todo - get parameter
    private val artistId: String by lazy { ArtistFragmentArgs.fromBundle(arguments!!).artistId }

    var artistUiData: ArtistUiObject? = null
    var albumsList: List<Listable>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        (activity as BaseActivity).presentationComponent.inject(this@ArtistFragment)
        postponeEnterTransition()
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        setEnterSharedElementCallback(object : SharedElementCallback() {
            override fun onMapSharedElements(names: MutableList<String>?, sharedElements: MutableMap<String, View>?) {

                sharedElements!![getString(R.string.image_transition)] = viewMvc.containerView?.findViewById(R.id.artist_image)!!
                sharedElements!![getString(R.string.transition_title)] = viewMvc.containerView?.findViewById(R.id.artist_title)!!

            }
        })

        return viewMvc.containerView
    }


    override fun onStart() {
        super.onStart()
        viewMvc.registerListener(this)

        if (artistUiData == null) {

            artistDetailsUseCase.apply {
                setCall(spotifyApi.getArtistInfo(artistId = artistId))
                makeCall(object : FetchUseCase.ResponseListener<ArtistSchema> {
                    override fun onResponseSuccess(response: ArtistSchema) {
                        artistUiData = ArtistUiObject().apply { setData(response) }
                        viewMvc.setArtistData(artistUiData!!)
                        startPostponedEnterTransition()
                    }

                    override fun onFail() {
                        handleApiError("error getting artist data")
                    }

                })
            }

        }

        if (albumsList == null) {

            artistAlbumsUseCase.apply {
                setCall(spotifyApi.getArtistAlbums(artistId = artistId))
                makeCall(object : FetchUseCase.ResponseListener<AlbumsSearchSchema> {
                    override fun onResponseSuccess(response: AlbumsSearchSchema) {
                        albumsList = constructAlbumSection(response)
                        viewMvc.setArtistAlbums(albumsList!!)
                    }

                    override fun onFail() {
                        handleApiError("error getting artist albums")
                    }

                })
            }
        }

    }

    override fun onStop() {
        super.onStop()
        viewMvc.unregisterListener(this)
        artistAlbumsUseCase.abortCall()
        artistDetailsUseCase.abortCall()
    }


    override fun onAlbumClicked(album: Selectable) {
        //todo - implement new transitions
        val options = ActivityOptions.makeSceneTransitionAnimation(activity, *album.sharedElements)
//        when (album) {
//            is Album -> startActivity(AlbumActivityView.intentBuilder(this, album.id), options.toBundle())
//        }


        val extras = FragmentNavigator.Extras.Builder().apply {
            album.sharedElements.forEach {
                this.addSharedElement(it.first, it.second)
            }
        }.build()

        when (album) {
            is Album -> {
                val action = ArtistFragmentDirections.actionGotoAlbumFragment(album.id)
                Navigation.findNavController(viewMvc.containerView!!).navigate(action, extras)

            }
        }


    }
}