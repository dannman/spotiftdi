package il.co.procyon.spotifydi.construction.presentation

import dagger.Subcomponent
import il.co.procyon.spotifydi.ui.albumActivity.AlbumActivityView
import il.co.procyon.spotifydi.ui.albumActivity.AlbumFragment
import il.co.procyon.spotifydi.ui.artistActivity.ArtistActivityView
import il.co.procyon.spotifydi.ui.artistActivity.ArtistFragment
import il.co.procyon.spotifydi.ui.mainActivity.MainActivityView
import il.co.procyon.spotifydi.ui.mainActivity.MainFragment

@Subcomponent(modules = [UseCaseModule::class, MvcModule::class])
interface PresentationComponent {

    fun inject(mainActivityView: MainActivityView)
    fun inject(albumActivityView: AlbumActivityView)
    fun inject(artistActivityView: ArtistActivityView)

    fun inject(mainFragment: MainFragment)
    fun inject(artistFragment: ArtistFragment)
    fun inject(albumFragment: AlbumFragment)




}