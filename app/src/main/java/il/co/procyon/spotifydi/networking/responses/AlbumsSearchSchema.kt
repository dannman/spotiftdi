package il.co.procyon.spotifydi.networking.responses

import com.google.gson.annotations.SerializedName

data class AlbumsSearchSchema(
        @SerializedName("href")
        val href: String = "",
        @SerializedName("items")
        val items: List<Item> = listOf(),
        @SerializedName("limit")
        val limit: Int = 0,
        @SerializedName("next")
        val next: String = "",
        @SerializedName("offset")
        val offset: Int = 0,
        @SerializedName("previous")
        val previous: Any = Any(),
        @SerializedName("total")
        val total: Int = 0
) {
    data class Item(
            @SerializedName("album_group")
            val albumGroup: String = "",
            @SerializedName("album_type")
            val albumType: String = "",
            @SerializedName("artists")
            val artists: List<Artist> = listOf(),
            @SerializedName("available_markets")
            val availableMarkets: List<String> = listOf(),
            @SerializedName("external_urls")
            val externalUrls: ExternalUrls = ExternalUrls(),
            @SerializedName("href")
            val href: String = "",
            @SerializedName("id")
            val id: String = "",
            @SerializedName("images")
            val images: List<Image> = listOf(),
            @SerializedName("name")
            val name: String = "",
            @SerializedName("release_date")
            val releaseDate: String = "",
            @SerializedName("release_date_precision")
            val releaseDatePrecision: String = "",
            @SerializedName("type")
            val type: String = "",
            @SerializedName("uri")
            val uri: String = "",
            @SerializedName("total_tracks")
            val totalTracks: Int = 0
    ) {
        data class Artist(
                @SerializedName("external_urls")
                val externalUrls: ExternalUrls = ExternalUrls(),
                @SerializedName("href")
                val href: String = "",
                @SerializedName("id")
                val id: String = "",
                @SerializedName("name")
                val name: String = "",
                @SerializedName("type")
                val type: String = "",
                @SerializedName("uri")
                val uri: String = ""
        ) {
            data class ExternalUrls(
                    @SerializedName("spotify")
                    val spotify: String = ""
            )
        }

        data class Image(
                @SerializedName("height")
                val height: Int = 0,
                @SerializedName("url")
                val url: String = "",
                @SerializedName("width")
                val width: Int = 0
        )

        data class ExternalUrls(
                @SerializedName("spotify")
                val spotify: String = ""
        )
    }
}