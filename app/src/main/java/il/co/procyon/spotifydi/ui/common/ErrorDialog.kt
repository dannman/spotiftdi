package il.co.procyon.spotifydi.ui.common

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

class ErrorDialog : DialogFragment() {

    companion object {

        private const val ERR_TITLE = "ERR_TITLE"
        private const val default_title = "Error loading service"

        private fun newInstannce(title: String = default_title): ErrorDialog {
            val args = Bundle()
            args.putString(ERR_TITLE, title)
            return ErrorDialog().apply {
                arguments = args
            }
        }

        fun showDilaog(transactionManager: FragmentManager
                       , title: String = default_title
                       , tag: String = ""
                       , onRetryClick: (() -> Unit)? = null) {
            newInstannce(title).apply {
                onRetryClick?.let {
                    onRetry = onRetryClick
                }

                this.show(transactionManager, tag)
            }
        }
    }

    var onRetry: (() -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context).apply {
            setTitle(arguments?.get(ERR_TITLE) as String)
            setMessage("Error Loading service")
            setNegativeButton("Ok") { dialog, _ -> dialog.dismiss() }
            onRetry?.let {
                setPositiveButton("Retry") { dialog, _ ->
                    onRetry!!.invoke()
                    dialog.dismiss()
                }
            }

        }.create()
    }


}