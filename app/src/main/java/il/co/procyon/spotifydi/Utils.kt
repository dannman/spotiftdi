package il.co.procyon.spotifydi

import android.app.Activity
import android.util.Base64
import android.view.View
import android.view.inputmethod.InputMethodManager


fun produceBase64AccessKey(clientId: String, clientSecret: String): String{
    val encodeInput = "$clientId:$clientSecret"
    val encodedKeys = Base64.encodeToString(encodeInput.toByteArray(), Base64.NO_WRAP)
    return "Basic $encodedKeys"
}

fun View.hideKeyboard(){
    (this.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager).apply {
        hideSoftInputFromWindow(this@hideKeyboard.windowToken, 0)
    }

}