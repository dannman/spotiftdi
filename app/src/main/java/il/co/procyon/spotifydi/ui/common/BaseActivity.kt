package il.co.procyon.spotifydi.ui.common

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.transition.Explode
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import il.co.procyon.spotifydi.construction.application.ApplicationComponent
import il.co.procyon.spotifydi.construction.presentation.MvcModule
import il.co.procyon.spotifydi.construction.presentation.PresentationComponent
import il.co.procyon.spotifydi.networking.SpotifyApi
import il.co.procyon.spotifydi.ui.root.App
import il.co.procyon.spotifydi.ui.root.ViewMvcFactory
import javax.inject.Inject

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    private val applicationComponent: ApplicationComponent by lazy { (application as App).applicationComponent }

    val presentationComponent: PresentationComponent by lazy {
        applicationComponent.newPresentationComponent(MvcModule(this))
    }

    @Inject
    lateinit var spotifyApi: SpotifyApi


    fun handleApiError(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        if (!isFinishing) {
            ErrorDialog.showDilaog(supportFragmentManager, msg)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(window) {
            requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
            exitTransition = Explode()
        }

    }
}