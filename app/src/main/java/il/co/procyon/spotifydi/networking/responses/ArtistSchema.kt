package il.co.procyon.spotifydi.networking.responses

import com.google.gson.annotations.SerializedName

data class ArtistSchema(
        @SerializedName("external_urls")
        val externalUrls: ExternalUrls = ExternalUrls(),
        @SerializedName("followers")
        val followers: Followers = Followers(),
        @SerializedName("genres")
        val genres: List<String> = listOf(),
        @SerializedName("href")
        val href: String = "",
        @SerializedName("id")
        val id: String = "",
        @SerializedName("images")
        val images: List<Image> = listOf(),
        @SerializedName("name")
        val name: String = "",
        @SerializedName("popularity")
        val popularity: Int = 0,
        @SerializedName("type")
        val type: String = "",
        @SerializedName("uri")
        val uri: String = ""
) {
    data class Followers(
            @SerializedName("href")
            val href: Any = Any(),
            @SerializedName("total")
            val total: Int = 0
    )

    data class Image(
            @SerializedName("height")
            val height: Int = 0,
            @SerializedName("url")
            val url: String = "",
            @SerializedName("width")
            val width: Int = 0
    )

    data class ExternalUrls(
            @SerializedName("spotify")
            val spotify: String = ""
    )
}