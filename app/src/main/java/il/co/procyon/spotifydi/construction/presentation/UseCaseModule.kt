package il.co.procyon.spotifydi.construction.presentation

import dagger.Module
import dagger.Provides
import il.co.procyon.spotifydi.networking.FetchUseCase
import il.co.procyon.spotifydi.networking.responses.*

@Module
class UseCaseModule {


    @Provides
    fun getSearchUseCase(): FetchUseCase<SearchResponseSchema> {
        return FetchUseCase()
    }

    @Provides
    fun getTokenUseCase(): FetchUseCase<TokenResponseSchema> {
        return FetchUseCase()
    }

    @Provides
    fun getArtistUseCase(): FetchUseCase<ArtistSchema> {
        return FetchUseCase()
    }

    @Provides
    fun getArtistAlbumsUseCase(): FetchUseCase<AlbumsSearchSchema> {
        return FetchUseCase()
    }
    @Provides
    fun getAlbumUseCase(): FetchUseCase<AlbumSchema> {
        return FetchUseCase()
    }
}