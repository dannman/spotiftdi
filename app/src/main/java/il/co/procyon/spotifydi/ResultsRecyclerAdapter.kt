package il.co.procyon.spotifydi

import android.content.Context
import android.transition.Transition
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.TransitionRes
import androidx.core.view.ViewCompat
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import il.co.procyon.spotifydi.databinding.CellAlbumBinding
import il.co.procyon.spotifydi.databinding.CellArtistBinding
import il.co.procyon.spotifydi.databinding.CellCategoryTitleBinding
import il.co.procyon.spotifydi.databinding.CellTrackBinding
import il.co.procyon.spotifydi.uiObjects.*
import java.util.zip.Inflater
import kotlin.properties.Delegates
import kotlin.properties.Delegates.observable

class ResultsRecyclerAdapter(context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private val inflater: LayoutInflater = LayoutInflater.from(context)


    enum class Type(val typeNum: Int) {
        HEADER(0),
        ARTIST(1),
        ALBUM(2),
        TRACK(3)
    }

    var items: List<Listable> by observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    var onItemClick: ((Selectable) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        inflater?.let {

            return when (viewType) {
                Type.HEADER.typeNum -> HeaderVH(CellCategoryTitleBinding.inflate(inflater, parent, false))
                Type.ARTIST.typeNum -> ArtistVH(CellArtistBinding.inflate(inflater, parent, false))
                Type.ALBUM.typeNum -> AlbumVH(CellAlbumBinding.inflate(inflater, parent, false))
                Type.TRACK.typeNum -> TrackVH(CellTrackBinding.inflate(inflater, parent, false))

                else -> HeaderVH(CellCategoryTitleBinding.inflate(inflater, parent, false))
            }
//        }
    }


    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        when {
            holder is HeaderVH && item is Header -> holder.binder.header = item
            holder is AlbumVH && item is Album -> holder.binder.album = item
            holder is ArtistVH && item is Artist -> holder.binder.artistObject = item
            holder is TrackVH && item is Track -> holder.binder.trackItem = item
        }

        if (item is Selectable) {

            item.sharedElements = when(holder){
                is AlbumVH -> arrayOf(
                        android.util.Pair(holder.binder.albumCover as View, ViewCompat.getTransitionName(holder.binder.albumCover)?:""),
                        android.util.Pair(holder.binder.title as View, ViewCompat.getTransitionName(holder.binder.title)?:"")
                )
                is ArtistVH -> arrayOf(
                        android.util.Pair(holder.binder.albumCover as View, ViewCompat.getTransitionName(holder.binder.albumCover)?:""),
                        android.util.Pair(holder.binder.artist as View, ViewCompat.getTransitionName(holder.binder.artist)?:"")
                )
                else -> emptyArray()
            }

            item.transitionView = when (holder) {
                is AlbumVH -> holder.binder.albumCover
                is ArtistVH -> holder.binder.albumCover
                is TrackVH -> holder.binder.albumCover
                else -> null
            }
            holder.itemView.setOnClickListener{onItemClick?.invoke(item)}
        }
    }




    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return when (item) {
            is Header -> Type.HEADER.typeNum
            is Artist -> Type.ARTIST.typeNum
            is Album -> Type.ALBUM.typeNum
            is Track -> Type.TRACK.typeNum
            else -> -1

        }
    }

    class HeaderVH(val binder: CellCategoryTitleBinding) : RecyclerView.ViewHolder(binder.root)

    class AlbumVH(val binder: CellAlbumBinding) : RecyclerView.ViewHolder(binder.root)

    class ArtistVH(val binder: CellArtistBinding) : RecyclerView.ViewHolder(binder.root)

    class TrackVH(val binder: CellTrackBinding) : RecyclerView.ViewHolder(binder.root)


}