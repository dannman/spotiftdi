package il.co.procyon.spotifydi.ui.artistActivity

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.SharedElementCallback
import il.co.procyon.spotifydi.R
import il.co.procyon.spotifydi.constructAlbumSection
import il.co.procyon.spotifydi.networking.FetchUseCase
import il.co.procyon.spotifydi.networking.responses.AlbumsSearchSchema
import il.co.procyon.spotifydi.networking.responses.ArtistSchema
import il.co.procyon.spotifydi.ui.albumActivity.AlbumActivityView
import il.co.procyon.spotifydi.ui.common.BaseActivity
import il.co.procyon.spotifydi.uiObjects.Album
import il.co.procyon.spotifydi.uiObjects.ArtistUiObject
import il.co.procyon.spotifydi.uiObjects.Listable
import il.co.procyon.spotifydi.uiObjects.Selectable
import javax.inject.Inject

class ArtistActivityView : BaseActivity(), ArtistViewMvc.Listener {

    companion object {
        private const val ARTIST_OBJ = "ARTIST_OBJ"

        fun intentBuilder(context: Context, artistId: String): Intent {
            val intent = Intent(context, ArtistActivityView::class.java)
            intent.putExtra(ARTIST_OBJ, artistId)
            return intent
        }

    }

    private val artistId: String by lazy { intent.getStringExtra(ARTIST_OBJ) }


    @Inject
    lateinit var viewMvc: ArtistViewMvc

    @Inject
    lateinit var artistDetailsUseCase: FetchUseCase<ArtistSchema>
    @Inject
    lateinit var artistAlbumsUseCase: FetchUseCase<AlbumsSearchSchema>

    var artistUiData: ArtistUiObject? = null
    var albumsList: List<Listable>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presentationComponent.inject(this)
        postponeEnterTransition()

        val startTextSize = intent.getFloatExtra("size", 0f)
        var endTextSize = 0f

        setContentView(viewMvc.containerView)


        setEnterSharedElementCallback(object : SharedElementCallback() {
            override fun onMapSharedElements(names: MutableList<String>?, sharedElements: MutableMap<String, View>?) {

                sharedElements!![getString(R.string.image_transition)] = viewMvc.containerView?.findViewById(R.id.artist_image)!!
                sharedElements!![getString(R.string.transition_title)] = viewMvc.containerView?.findViewById(R.id.artist_title)!!

            }
        })


    }

    override fun onStart() {
        super.onStart()
        viewMvc.registerListener(this)

        if (artistUiData == null) {

            artistDetailsUseCase.apply {
                setCall(spotifyApi.getArtistInfo(artistId = artistId))
                makeCall(object : FetchUseCase.ResponseListener<ArtistSchema> {
                    override fun onResponseSuccess(response: ArtistSchema) {
                        artistUiData = ArtistUiObject().apply { setData(response) }
                        viewMvc.setArtistData(artistUiData!!)
                        startPostponedEnterTransition()
                    }

                    override fun onFail() {
                        handleApiError("error getting artist data")
                    }

                })
            }

        }

        if (albumsList == null) {

            artistAlbumsUseCase.apply {
                setCall(spotifyApi.getArtistAlbums(artistId = artistId))
                makeCall(object : FetchUseCase.ResponseListener<AlbumsSearchSchema> {
                    override fun onResponseSuccess(response: AlbumsSearchSchema) {
                        albumsList = constructAlbumSection(response)
                        viewMvc.setArtistAlbums(albumsList!!)
                    }

                    override fun onFail() {
                        handleApiError("error getting artist albums")
                    }

                })
            }
        }

    }

    override fun onStop() {
        super.onStop()
        viewMvc.unregisterListener(this)
        artistAlbumsUseCase.abortCall()
        artistDetailsUseCase.abortCall()
    }


    override fun onAlbumClicked(album: Selectable) {
        val options = ActivityOptions.makeSceneTransitionAnimation(this, *album.sharedElements)
        when (album) {
            is Album -> startActivity(AlbumActivityView.intentBuilder(this, album.id), options.toBundle())
        }
    }
}