package il.co.procyon.spotifydi.networking

import il.co.procyon.spotifydi.CredentialManager
import il.co.procyon.spotifydi.networking.responses.*
import retrofit2.Call
import retrofit2.http.*

interface SpotifyApi {
    @FormUrlEncoded
    @POST("https://accounts.spotify.com/api/token")
    fun getToken(@Header("Authorization") accessKey: String, @Field(value= "grant_type", encoded = false) grant_type: String = "client_credentials" ): Call<TokenResponseSchema>

    @GET("search")
    fun search(@Header("Authorization") token: String = "Bearer ${CredentialManager.token.tokenVal}", @Query("q") query: String, @Query("type") type: String): Call<SearchResponseSchema>

    @GET("artists/{id}")
    fun getArtistInfo(@Header("Authorization") token: String = "Bearer ${CredentialManager.token.tokenVal}", @Path("id") artistId: String): Call<ArtistSchema>

    @GET("artists/{id}/albums")
    fun getArtistAlbums(@Header("Authorization") token: String = "Bearer ${CredentialManager.token.tokenVal}", @Path("id") artistId: String): Call<AlbumsSearchSchema>

    @GET("artists/{id}/top-tracks")
    fun getArtistTopTracks(@Header("Authorization") token: String = "Bearer ${CredentialManager.token.tokenVal}", @Path("id") artistId: String): Call<Any>

    @GET("albums/{id}")
    fun getAlbumInfo(@Header("Authorization") token: String = "Bearer ${CredentialManager.token.tokenVal}", @Path("id") albumId: String): Call<AlbumSchema>

}