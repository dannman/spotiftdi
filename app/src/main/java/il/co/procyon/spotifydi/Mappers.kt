package il.co.procyon.spotifydi

import il.co.procyon.spotifydi.networking.responses.AlbumsSearchSchema
import il.co.procyon.spotifydi.networking.responses.SearchResponseSchema
import il.co.procyon.spotifydi.uiObjects.*

fun processSearchSchema(schema: SearchResponseSchema): List<Listable> {
    val results = mutableListOf<Listable>()
    results.addAll(constructAlbumSection(schema.albums))

    if (schema.artists.items.isNotEmpty()) {
        results.add(Header().apply { header.set("Artists") })

        schema.artists.items.forEach { schemaArtist ->
            results.add(Artist(schemaArtist.id).apply {
                artistName.set(schemaArtist.name)
                coverArtUrl.set(schemaArtist.images.firstOrNull()?.url ?: "null")
            })

        }
    }

    if (schema.tracks.items.isNotEmpty()) {
        results.add(Header().apply { header.set("Tracks") })

        schema.tracks.items.forEach { schemaTracks ->
            results.add(Track(schemaTracks.id).apply {
                albumName.set(schemaTracks.album.name)
                artistName.set(schemaTracks.artists.firstOrNull()?.name?:"Not Available")
                trackName.set(schemaTracks.name)
                coverArtUrl.set(schemaTracks.album.images.firstOrNull()?.url?:"null")
            })
        }
    }

    return results


}

 fun constructAlbumSection(schema: AlbumsSearchSchema): List<Listable> {
    val tempList = mutableListOf<Listable>()
     if (schema.items.isNotEmpty()) {
         tempList.add(Header().apply { header.set("Albums") })
        schema.items.forEach { schemaAlbums ->
            tempList.add(Album(schemaAlbums.id).apply {
                albumName.set(schemaAlbums.name)
                artistName.set(schemaAlbums.artists.firstOrNull()?.name ?: "Not available")
                coverArtUrl.set(schemaAlbums.images.firstOrNull()?.url ?: "null")
            })
        }

    }
     return tempList.toList()
}
