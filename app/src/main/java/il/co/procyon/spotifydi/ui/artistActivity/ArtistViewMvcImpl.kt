package il.co.procyon.spotifydi.ui.artistActivity

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import il.co.procyon.spotifydi.R
import il.co.procyon.spotifydi.ResultsRecyclerAdapter
import il.co.procyon.spotifydi.databinding.ActivityArtistBinding
import il.co.procyon.spotifydi.ui.baseViewMvc.BaseViewMvc
import il.co.procyon.spotifydi.uiObjects.ArtistUiObject
import il.co.procyon.spotifydi.uiObjects.Listable
import il.co.procyon.spotifydi.uiObjects.Selectable
import kotlinx.android.synthetic.main.activity_artist.*
import kotlinx.android.synthetic.main.cell_track.*

class ArtistViewMvcImpl(override val containerView: View?) : BaseViewMvc<ArtistViewMvc.Listener>(), ArtistViewMvc {
    companion object {

        fun instantiateMvc(context: Context, root: ViewGroup?  =null): ArtistViewMvcImpl{
            return ArtistViewMvcImpl(LayoutInflater.from(context).inflate(R.layout.activity_artist, root))
        }

    }

    private lateinit var binder: ActivityArtistBinding
    private val artistUiObject = ArtistUiObject()
    private lateinit var resultsRecyclerAdapter :ResultsRecyclerAdapter

    init {
        content_recycler?.apply {
            resultsRecyclerAdapter = ResultsRecyclerAdapter(containerView?.context!!)

            resultsRecyclerAdapter.items = artistUiObject.itemsList
            adapter = resultsRecyclerAdapter
            layoutManager = LinearLayoutManager(containerView.context!!)
        }
    }

    override fun registerListener(listener: ArtistViewMvc.Listener) {
        super.registerListener(listener)
//        binder = ActivityArtistBinding.bind(containerView?:return)
        binder = DataBindingUtil.getBinding(containerView!!)?:ActivityArtistBinding.bind(containerView)
        binder.artist = artistUiObject
        resultsRecyclerAdapter.onItemClick = onItemSelect
    }

    override fun setArtistData(artist: ArtistUiObject) {
        artistUiObject.apply {
            imageUrl.set(artist.imageUrl.get())
            artistName.set(artist.artistName.get())
            genres.set(artist.genres.get())
        }
    }

    override fun setArtistAlbums(albums: List<Listable>) {
        artistUiObject.itemsList.apply {
            clear()
            addAll(albums)
            resultsRecyclerAdapter.notifyDataSetChanged()
        }
    }

    private val onItemSelect: (Selectable) -> Unit = { it ->
        getListeners().forEach { listener -> listener.onAlbumClicked(it) }
    }






    }
