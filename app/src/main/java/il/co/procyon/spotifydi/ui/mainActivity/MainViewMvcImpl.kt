package il.co.procyon.spotifydi.ui.mainActivity

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import il.co.procyon.spotifydi.R
import il.co.procyon.spotifydi.ResultsRecyclerAdapter
import il.co.procyon.spotifydi.hideKeyboard
import il.co.procyon.spotifydi.networking.responses.SearchResponseSchema
import il.co.procyon.spotifydi.processSearchSchema
import il.co.procyon.spotifydi.ui.baseViewMvc.BaseViewMvc
import il.co.procyon.spotifydi.uiObjects.Selectable
import kotlinx.android.synthetic.main.activity_main.*

class MainViewMvcImpl(override val containerView: View?) : BaseViewMvc<MainViewMvc.Listener>(), MainViewMvc {

    private var resultsRecyclerAdapter: ResultsRecyclerAdapter? = null

    init {
        if (resultsRecyclerAdapter == null) {
            resultsRecyclerAdapter = ResultsRecyclerAdapter(containerView?.context!!).apply {
                onItemClick = onItemClickImpl
            }
        }
        results_list.apply {
            layoutManager = LinearLayoutManager(containerView?.context!!)
            adapter = resultsRecyclerAdapter
        }

        btn_get_token.setOnClickListener {

            getListeners().forEach { listener -> listener.onGetTokenClick() }
        }

        search_button?.setOnClickListener {
            onSearch.invoke()
            search_field.hideKeyboard()
        }

        search_field?.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                onSearch.invoke()
                search_field.hideKeyboard()
                return@setOnEditorActionListener true
            }
            false
        }
    }


    override fun registerListener(listener: MainViewMvc.Listener) {
        super.registerListener(listener)

        resultsRecyclerAdapter!!.onItemClick = onItemClickImpl

    }


    override fun bindSearchResponses(results: SearchResponseSchema) {
        resultsRecyclerAdapter?.items = processSearchSchema(results)
    }

    override fun storeToken(token: String, expiresIn: Long) {
        Toast.makeText(containerView?.context, "token: $token}", Toast.LENGTH_LONG).show()

    }

    override fun hasTokenBeenFetched(hasToken: Boolean) {
        search_button.isEnabled = hasToken
        btn_get_token.isEnabled = !hasToken
    }

    private val onItemClickImpl: (selectable: Selectable) -> Unit = { selectable ->
        getListeners().forEach { listener ->
            listener.onListItemClick(selectable)
        }
    }

    private val onSearch: () -> Unit = {
        //        resultsRecyclerAdapter?.items = testResults
        val query = search_field.text.toString()
        val types = getTypeQuery()
        if (validateQuery(query, types)) {
            getListeners().forEach { it.onSearchClick(query, types) }
        } else {
            Toast.makeText(containerView?.context, "Please select at least 1 type, and non-blank query", Toast.LENGTH_LONG).show()
        }
    }

    private fun validateQuery(queryString: String, type: String): Boolean {
        return !queryString.isNullOrBlank() && !type.isNullOrBlank()
    }

    private fun getTypeQuery(): String {
        val typeList = mutableListOf<String>()
        if (albums_cb.isChecked) typeList.add("album")
        if (artists_cb.isChecked) typeList.add("artist")
        if (tracks_cb.isChecked) typeList.add("track")

        return typeList.joinToString(separator = ",")
    }


    companion object {
        fun instantiateMvc(context: Context, root: ViewGroup? = null): MainViewMvcImpl {
            return MainViewMvcImpl(LayoutInflater.from(context).inflate(R.layout.activity_main, root))
        }
    }
}