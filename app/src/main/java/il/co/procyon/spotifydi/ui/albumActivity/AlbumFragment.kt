package il.co.procyon.spotifydi.ui.albumActivity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import il.co.procyon.spotifydi.networking.FetchUseCase
import il.co.procyon.spotifydi.networking.responses.AlbumSchema
import il.co.procyon.spotifydi.ui.common.BaseActivity
import il.co.procyon.spotifydi.ui.common.BaseFragment
import il.co.procyon.spotifydi.uiObjects.AlbumUiObject
import javax.inject.Inject

class AlbumFragment: BaseFragment(),  AlbumViewMvc.Listener  {

    @Inject
    lateinit var albumMvc: AlbumViewMvc

    @Inject
    lateinit var getAlbumsUseCase: FetchUseCase<AlbumSchema>


    val id: String by lazy { AlbumFragmentArgs.fromBundle(arguments!!).albumId }

    override fun onCreate(savedInstanceState: Bundle?) {
        (activity as BaseActivity).presentationComponent.inject(this@AlbumFragment)
        postponeEnterTransition()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return albumMvc.containerView
    }

    override fun onStart() {
        super.onStart()
        albumMvc.registerListener(this)

        getAlbumsUseCase.apply {
            setCall(spotifyApi.getAlbumInfo(albumId = id))
            makeCall(albumListener)
        }

    }

    private val albumListener = object : FetchUseCase.ResponseListener<AlbumSchema> {
        override fun onResponseSuccess(response: AlbumSchema) {
            albumMvc.setAlbumData(AlbumUiObject(response))
            startPostponedEnterTransition()
        }

        override fun onFail() {
            handleApiError("error loading album")
        }

    }

    override fun onStop() {
        super.onStop()
        albumMvc.unregisterListener(this)
        getAlbumsUseCase.abortCall()

    }
}