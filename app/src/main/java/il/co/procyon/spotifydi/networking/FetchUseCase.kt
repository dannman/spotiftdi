package il.co.procyon.spotifydi.networking

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FetchUseCase<RESPONSE>(private var apiCall: Call<RESPONSE>? = null) {

    interface ResponseListener<_RESPONSE> {
        fun onResponseSuccess(response: _RESPONSE)
        fun onFail()
    }


    fun setCall(call: Call<RESPONSE>?){
        apiCall = call
    }
    fun makeCall( listener: ResponseListener<RESPONSE>) {

//        abortCall()
        apiCall?.enqueue(object : Callback<RESPONSE> {
            override fun onFailure(call: Call<RESPONSE>, t: Throwable) {
                listener.onFail()
            }

            override fun onResponse(call: Call<RESPONSE>, response: Response<RESPONSE>) {
                if (response.isSuccessful && response.body() != null) {
                    listener.onResponseSuccess(response.body()!!)
                } else {
                    listener.onFail()
                }
            }
        })
    }


    fun abortCall() {
        if (apiCall?.isCanceled != true && apiCall?.isExecuted != true) {
            apiCall?.cancel()
        }
    }

}