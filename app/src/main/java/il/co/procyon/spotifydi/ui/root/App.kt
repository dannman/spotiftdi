package il.co.procyon.spotifydi.ui.root

import android.app.Application
import androidx.lifecycle.Lifecycle
import il.co.procyon.spotifydi.construction.application.ApplicationComponent
import il.co.procyon.spotifydi.construction.application.ApplicationModule
import il.co.procyon.spotifydi.construction.application.DaggerApplicationComponent

class App : Application() {


    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()

    }
}