package il.co.procyon.spotifydi

import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import il.co.procyon.spotifydi.ui.albumActivity.AlbumViewMvcImpl
import il.co.procyon.spotifydi.uiObjects.AlbumUiObject


@BindingAdapter("imageUrl")
fun ImageView.setImageUrl(url: String?) {
    Log.d("BindingAdapters", "setImageUrl: $url")
    Picasso.get().load(url).into(this@setImageUrl)
}

@set:BindingAdapter("gone")
var View.gone: Boolean
    get() = visibility == View.GONE
    set(value) {
        visibility = if (value) View.GONE else View.VISIBLE
    }

@set:BindingAdapter("invisible")
var View.invisible: Boolean
    get() = visibility == View.INVISIBLE
    set(value) {
        visibility = if (value) View.INVISIBLE else View.VISIBLE
    }

@BindingAdapter("bindTracks")
fun RecyclerView.setupAlbumTracks(tracks: List<AlbumUiObject.AlbumTrack>?) {
    tracks?.let {
        if (layoutManager == null) {
            this.layoutManager = LinearLayoutManager(this.context)
        }
        if (adapter == null) {
            val adapter = AlbumViewMvcImpl.TracksAdapter()
            this.adapter = adapter
        }
        (adapter as AlbumViewMvcImpl.TracksAdapter).trackList = tracks
    }
}
