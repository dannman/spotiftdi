package il.co.procyon.spotifydi.ui.baseViewMvc

import il.co.procyon.spotifydi.common.BaseObservable

abstract class BaseViewMvc<LISTENER_TYPE> : BaseObservable<LISTENER_TYPE>()