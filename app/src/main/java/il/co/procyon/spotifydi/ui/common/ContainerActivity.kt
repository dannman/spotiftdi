package il.co.procyon.spotifydi.ui.common

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import il.co.procyon.spotifydi.R
import il.co.procyon.spotifydi.ui.mainActivity.MainFragment

class ContainerActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)
    }
}
