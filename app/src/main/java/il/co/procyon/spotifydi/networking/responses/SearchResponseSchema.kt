package il.co.procyon.spotifydi.networking.responses

import com.google.gson.annotations.SerializedName

data class SearchResponseSchema(
        @SerializedName("albums")
        val albums: AlbumsSearchSchema = AlbumsSearchSchema(),
        @SerializedName("artists")
        val artists: Artists = Artists(),
        @SerializedName("tracks")
        val tracks: Tracks = Tracks()
) {
    data class Artists(
            @SerializedName("href")
            val href: String = "",
            @SerializedName("items")
            val items: List<Item> = listOf(),
            @SerializedName("limit")
            val limit: Int = 0,
            @SerializedName("next")
            val next: String = "",
            @SerializedName("offset")
            val offset: Int = 0,
            @SerializedName("previous")
            val previous: Any = Any(),
            @SerializedName("total")
            val total: Int = 0
    ) {
        data class Item(
                @SerializedName("external_urls")
                val externalUrls: ExternalUrls = ExternalUrls(),
                @SerializedName("followers")
                val followers: Followers = Followers(),
                @SerializedName("genres")
                val genres: List<Any> = listOf(),
                @SerializedName("href")
                val href: String = "",
                @SerializedName("id")
                val id: String = "",
                @SerializedName("images")
                val images: List<Image> = listOf(),
                @SerializedName("name")
                val name: String = "",
                @SerializedName("popularity")
                val popularity: Int = 0,
                @SerializedName("type")
                val type: String = "",
                @SerializedName("uri")
                val uri: String = ""
        ) {
            data class Followers(
                    @SerializedName("href")
                    val href: Any = Any(),
                    @SerializedName("total")
                    val total: Int = 0
            )

            data class Image(
                    @SerializedName("height")
                    val height: Int = 0,
                    @SerializedName("url")
                    val url: String = "",
                    @SerializedName("width")
                    val width: Int = 0
            )

            data class ExternalUrls(
                    @SerializedName("spotify")
                    val spotify: String = ""
            )
        }
    }

    data class Tracks(
            @SerializedName("href")
            val href: String = "",
            @SerializedName("items")
            val items: List<Item> = listOf(),
            @SerializedName("limit")
            val limit: Int = 0,
            @SerializedName("next")
            val next: String = "",
            @SerializedName("offset")
            val offset: Int = 0,
            @SerializedName("previous")
            val previous: Any = Any(),
            @SerializedName("total")
            val total: Int = 0
    ) {
        data class Item(
                @SerializedName("album")
                val album: Album = Album(),
                @SerializedName("artists")
                val artists: List<Artist> = listOf(),
                @SerializedName("available_markets")
                val availableMarkets: List<String> = listOf(),
                @SerializedName("disc_number")
                val discNumber: Int = 0,
                @SerializedName("duration_ms")
                val durationMs: Int = 0,
                @SerializedName("explicit")
                val explicit: Boolean = false,
                @SerializedName("external_ids")
                val externalIds: ExternalIds = ExternalIds(),
                @SerializedName("external_urls")
                val externalUrls: ExternalUrls = ExternalUrls(),
                @SerializedName("href")
                val href: String = "",
                @SerializedName("id")
                val id: String = "",
                @SerializedName("is_local")
                val isLocal: Boolean = false,
                @SerializedName("name")
                val name: String = "",
                @SerializedName("popularity")
                val popularity: Int = 0,
                @SerializedName("preview_url")
                val previewUrl: Any = Any(),
                @SerializedName("track_number")
                val trackNumber: Int = 0,
                @SerializedName("type")
                val type: String = "",
                @SerializedName("uri")
                val uri: String = ""
        ) {
            data class ExternalIds(
                    @SerializedName("isrc")
                    val isrc: String = ""
            )

            data class Artist(
                    @SerializedName("external_urls")
                    val externalUrls: ExternalUrls = ExternalUrls(),
                    @SerializedName("href")
                    val href: String = "",
                    @SerializedName("id")
                    val id: String = "",
                    @SerializedName("name")
                    val name: String = "",
                    @SerializedName("type")
                    val type: String = "",
                    @SerializedName("uri")
                    val uri: String = ""
            ) {
                data class ExternalUrls(
                        @SerializedName("spotify")
                        val spotify: String = ""
                )
            }

            data class Album(
                    @SerializedName("album_type")
                    val albumType: String = "",
                    @SerializedName("artists")
                    val artists: List<Artist> = listOf(),
                    @SerializedName("available_markets")
                    val availableMarkets: List<String> = listOf(),
                    @SerializedName("external_urls")
                    val externalUrls: ExternalUrls = ExternalUrls(),
                    @SerializedName("href")
                    val href: String = "",
                    @SerializedName("id")
                    val id: String = "",
                    @SerializedName("images")
                    val images: List<Image> = listOf(),
                    @SerializedName("name")
                    val name: String = "",
                    @SerializedName("release_date")
                    val releaseDate: String = "",
                    @SerializedName("release_date_precision")
                    val releaseDatePrecision: String = "",
                    @SerializedName("total_tracks")
                    val totalTracks: Int = 0,
                    @SerializedName("type")
                    val type: String = "",
                    @SerializedName("uri")
                    val uri: String = ""
            ) {
                data class Artist(
                        @SerializedName("external_urls")
                        val externalUrls: ExternalUrls = ExternalUrls(),
                        @SerializedName("href")
                        val href: String = "",
                        @SerializedName("id")
                        val id: String = "",
                        @SerializedName("name")
                        val name: String = "",
                        @SerializedName("type")
                        val type: String = "",
                        @SerializedName("uri")
                        val uri: String = ""
                ) {
                    data class ExternalUrls(
                            @SerializedName("spotify")
                            val spotify: String = ""
                    )
                }

                data class Image(
                        @SerializedName("height")
                        val height: Int = 0,
                        @SerializedName("url")
                        val url: String = "",
                        @SerializedName("width")
                        val width: Int = 0
                )

                data class ExternalUrls(
                        @SerializedName("spotify")
                        val spotify: String = ""
                )
            }

            data class ExternalUrls(
                    @SerializedName("spotify")
                    val spotify: String = ""
            )
        }
    }

//    data class Albums(
//            @SerializedName("href")
//            val href: String = "",
//            @SerializedName("items")
//            val items: List<Item> = listOf(),
//            @SerializedName("limit")
//            val limit: Int = 0,
//            @SerializedName("next")
//            val next: String = "",
//            @SerializedName("offset")
//            val offset: Int = 0,
//            @SerializedName("previous")
//            val previous: Any = Any(),
//            @SerializedName("total")
//            val total: Int = 0
//    ) {
//        data class Item(
//                @SerializedName("album_type")
//                val albumType: String = "",
//                @SerializedName("artists")
//                val artists: List<Artist> = listOf(),
//                @SerializedName("available_markets")
//                val availableMarkets: List<String> = listOf(),
//                @SerializedName("external_urls")
//                val externalUrls: ExternalUrls = ExternalUrls(),
//                @SerializedName("href")
//                val href: String = "",
//                @SerializedName("id")
//                val id: String = "",
//                @SerializedName("images")
//                val images: List<Image> = listOf(),
//                @SerializedName("name")
//                val name: String = "",
//                @SerializedName("release_date")
//                val releaseDate: String = "",
//                @SerializedName("release_date_precision")
//                val releaseDatePrecision: String = "",
//                @SerializedName("total_tracks")
//                val totalTracks: Int = 0,
//                @SerializedName("type")
//                val type: String = "",
//                @SerializedName("uri")
//                val uri: String = ""
//        ) {
//            data class Artist(
//                    @SerializedName("external_urls")
//                    val externalUrls: ExternalUrls = ExternalUrls(),
//                    @SerializedName("href")
//                    val href: String = "",
//                    @SerializedName("id")
//                    val id: String = "",
//                    @SerializedName("name")
//                    val name: String = "",
//                    @SerializedName("type")
//                    val type: String = "",
//                    @SerializedName("uri")
//                    val uri: String = ""
//            ) {
//                data class ExternalUrls(
//                        @SerializedName("spotify")
//                        val spotify: String = ""
//                )
//            }
//
//            data class Image(
//                    @SerializedName("height")
//                    val height: Int = 0,
//                    @SerializedName("url")
//                    val url: String = "",
//                    @SerializedName("width")
//                    val width: Int = 0
//            )
//
//            data class ExternalUrls(
//                    @SerializedName("spotify")
//                    val spotify: String = ""
//            )
//        }
//    }
}