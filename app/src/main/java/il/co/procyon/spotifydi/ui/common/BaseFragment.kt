package il.co.procyon.spotifydi.ui.common

import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import il.co.procyon.spotifydi.FragTransition
import il.co.procyon.spotifydi.construction.application.ApplicationComponent
import il.co.procyon.spotifydi.construction.presentation.MvcModule
import il.co.procyon.spotifydi.construction.presentation.PresentationComponent
import il.co.procyon.spotifydi.networking.SpotifyApi
import il.co.procyon.spotifydi.ui.root.App
import javax.inject.Inject


open class BaseFragment : Fragment() {

    private val applicationComponent: ApplicationComponent by lazy { (activity!!.application as App).applicationComponent }

    val presentationComponent: PresentationComponent by lazy {
        applicationComponent.newPresentationComponent(MvcModule(context!!))
    }

    @Inject
    lateinit var spotifyApi: SpotifyApi


    fun handleApiError(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        if (activity?.isFinishing == true) {
            ErrorDialog.showDilaog(childFragmentManager, msg)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        exitTransition = android.transition.Explode()
//        sharedElementEnterTransition = FragTransition()
//        sharedElementReturnTransition = FragTransition()

    }
}